
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:pub_browser_dependent_model/css_property.dart' hide Duration;
import 'package:pub_base_model/directive.dart' as base;
import 'package:sample_dart_web_angular/src/ayu/status/css_background/css_background_status.dart';

import 'css_background_process.dart';

import 'css_background_process_switch.template.dart' as temp;

@Component(
    selector: 'ayu-css-background-switch',
    templateUrl: 'css_background_process_switch.html',
    directives: [
        NgFor
        ,NgSwitch
        ,NgSwitchWhen
        ,CssBackgroundProcess
    ],
    exports: [
    ],
)
class CssBackgroundProcessSwitch
    extends
        base.Directive
    implements
        OnInit
{

    static final template = temp.CssBackgroundProcessSwitchNgFactory;

    CssBackgroundProcessSwitch(this.el);
    final HtmlElement el;

    @ViewChild('switchContainer')
    HtmlElement? switchContainer;

    @Input()
    late CssBackgroundStatusList statusList;

    Object situation = 'initial';

    @override
    void ngOnInit() {
        if(switchContainer == null) return;
        switchContainer!.style.position = Position.absolute().toString();
        switchContainer!.style.top = '0';
        switchContainer!.style.bottom = '0';
        switchContainer!.style.left = '0';
        switchContainer!.style.right = '0';
    }

}

import 'dart:async';
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:sample_dart_web_angular/src/ayu/status/status_barrel.dart';
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_browser_dependent_model/css_property.dart' hide Duration;
import 'package:pub_base_model/directive.dart' as base;

import 'sprite_background_process.template.dart' as temp;

export 'sprite_background_process_switch.dart';

@Component(
    selector: 'ayu-sprite-background',
    template: '',
    directives: [
    ],
    exports: [
    ],
)
class SpriteBackgroundProcess 
    extends
        base.Directive
    implements
        OnInit
        ,AfterViewInit
        ,OnDestroy
{

    static final template = temp.SpriteBackgroundProcessNgFactory;

    SpriteBackgroundProcess(this.el);
    final HtmlElement el;

    @Input()
    late SpriteBackgroundStatus status;

    @Input()
    List<String>? afterTurnResult;

    Timer? _timer;

    @override
    void ngOnInit() {
        el.style.position = Position.absolute().toString();
        el.style.width = '100%';
        el.style.height = '100%';
        el.style.backgroundSize = 'auto auto';
        el.style.backgroundRepeat = 'no-repeat';
    }

    @override
    void ngAfterViewInit() {
        if (afterTurnResult == null) {
            spriteBackgroundProcess();
        } else {
            play(afterTurnResult!);
        }
    }

    @override
    void ngOnDestroy() {
        if (_timer != null) _timer!.cancel();
    }

    void spriteBackgroundProcess() {
        final imageElement = ImageElement()
            ..src = status.src.toString()
        ;

        imageElement.onLoad.listen((event) {
            final afterTurnResult = createResult(el, status, imageElement);
            play(afterTurnResult);
        });
    }

    static List<String> createResult(HtmlElement el, SpriteBackgroundStatus status, ImageElement imageElement) {

        final canvasWidth = el.clientWidth;
        final canvasHeight = el.clientHeight;
        // print(canvasWidth);
        // print(canvasHeight);

        final canvasElement = CanvasElement(width: canvasWidth, height: canvasHeight);
        final context2D = canvasElement.context2D;

        var originalSprite = <String>[];
        var afterRangeResult = <String>[];
        var afterTurnResult = <String>[];

        final beginPoint = (status.spriteSheetColumns.value * (status.rangeBeginY.value - 1)) + status.rangeBeginX.value - 1;

        final rangeNumber = <int>[];
        for (var ii = 0; ii <= status.rangeEndY.value - status.rangeBeginY.value; ii++) {
            for (var i = 0; i <= status.rangeEndX.value - status.rangeBeginX.value; i++) {
                rangeNumber.add(status.spriteSheetColumns.value * ii + beginPoint + i);
            }
        }

        var columnsCutInterval = imageElement.naturalWidth / status.spriteSheetColumns.value;
        var rowsCutInterval = imageElement.naturalHeight / status.spriteSheetRows.value;

        for (var i = 0; i < status.spriteSheetRows.value; i++) {
            for (var ii = 0; ii < status.spriteSheetColumns.value; ii++) {
                context2D.clearRect(0, 0, canvasElement.width!, canvasElement.height!);
                context2D.drawImageScaledFromSource(
                    imageElement,
                    columnsCutInterval * ii,
                    rowsCutInterval * i,
                    columnsCutInterval,
                    rowsCutInterval,
                    0,
                    0,
                    canvasElement.width!,
                    canvasElement.height!,
                );
                originalSprite.add(canvasElement.toDataUrl('image/png'));
                // print(canvasElement.toDataUrl('image/png'));
            }
        }

        rangeNumber.forEach((i) {
            afterRangeResult.add(originalSprite[i]);
        });

        status.turnList.forEach((i) {
            afterTurnResult.add(afterRangeResult[i.value]);
        });

        return afterTurnResult;
    }

    void play(List<String> afterTurnResult) {
        var counter = 0;
        /// sprite animation の situation が切り替わったときの、
        /// 1つ目の particle が表示されるまでの時間差を無くすための処理。
        el.style.backgroundImage = 'url(${afterTurnResult[counter]})';
        counter++;

        _timer = Timer.periodic(Duration(milliseconds: status.duration.value), (Timer timer) {
            /// afterRangeResult.length = 1 && afterTurnResult.lenth = 1 のとき、
            /// この位置に、「if(counter >= afterTurnResult.length) counter = 0;」が無ければだめ。
            if(counter >= afterTurnResult.length) counter = 0;
            
            el.style.backgroundImage = 'none';
            el.style.backgroundImage = 'url(${afterTurnResult[counter]})';
            counter++;
            // print(counter);
        });
    }

}
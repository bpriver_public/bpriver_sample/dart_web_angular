
import 'package:pub_base_model/model.dart';
import 'package:sample_dart_web_angular/src/ayu/status/status_barrel.dart';
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';

class ViewportConfiguration
    extends
        ObjectModel<SampleDartWebAngularModule>
{

    ViewportConfiguration(this.viewportStatus, this.focusStatus, [this.spriteBackgroundStatusList, this.cssBackgroundStatusList, this.imageBackgroundStatusList, ]);
    
    final ViewportStatus viewportStatus;
    final FocusStatus focusStatus;
    final SpriteBackgroundStatusList? spriteBackgroundStatusList;
    final CssBackgroundStatusList? cssBackgroundStatusList;
    final ImageBackgroundStatusList? imageBackgroundStatusList;

    @override
    Map<String, String> toPrimitive() {
        return {
            'viewportStatus': viewportStatus.toString(),
            'focusStatus': focusStatus.toString(),
            'spriteBackgroundStatusList': spriteBackgroundStatusList.toString(),
        };
    }

}
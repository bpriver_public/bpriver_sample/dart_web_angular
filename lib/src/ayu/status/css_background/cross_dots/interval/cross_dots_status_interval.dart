
import 'package:pub_base_model/model.dart';
import 'package:pub_base_model/num_state_model_action.dart';

class CrossDotsStatusInterval
    extends
        NumStateModel<num>
    with
        NumStateModelGetPx<num>
{

    /// dot の間隔
    /// 単位は px.
    CrossDotsStatusInterval(this.value);

    @override
    final num value;

}
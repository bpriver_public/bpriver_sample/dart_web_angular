
import 'package:pub_browser_dependent_model/css_property.dart';

class CrossDotsStatusBackgroundColor
    extends
        Color
{

    const CrossDotsStatusBackgroundColor.rgba(String value) : super.rgba(value);
    const CrossDotsStatusBackgroundColor.black() : super.black();
    const CrossDotsStatusBackgroundColor.blue() : super.blue();
    const CrossDotsStatusBackgroundColor.red() : super.red();
    const CrossDotsStatusBackgroundColor.purple() : super.purple();
    const CrossDotsStatusBackgroundColor.green() : super.green();
    const CrossDotsStatusBackgroundColor.yellow() : super.yellow();

}
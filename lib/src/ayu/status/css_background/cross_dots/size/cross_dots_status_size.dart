
import 'package:pub_base_model/model.dart';
import 'package:pub_base_model/num_state_model_action.dart';

class CrossDotsStatusSize
    extends
        NumStateModel<num>
    with
        NumStateModelGetPx<num>
{

    /// dot の大きさ
    /// 単位は px.
    CrossDotsStatusSize(this.value);

    @override
    final num value;

}
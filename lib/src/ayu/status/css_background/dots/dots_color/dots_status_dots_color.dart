
import 'package:pub_browser_dependent_model/css_property.dart';

class DotsStatusDotsColor
    extends
        Color
{

    const DotsStatusDotsColor.rgba(String value) : super.rgba(value);
    const DotsStatusDotsColor.black() : super.black();
    const DotsStatusDotsColor.blue() : super.blue();
    const DotsStatusDotsColor.red() : super.red();
    const DotsStatusDotsColor.purple() : super.purple();
    const DotsStatusDotsColor.green() : super.green();
    const DotsStatusDotsColor.yellow() : super.yellow();

}
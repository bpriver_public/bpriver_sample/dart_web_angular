
import 'package:pub_base_model/mutable.dart';

import 'css_background_status_situation.dart';

class MutableCssBackgroundStatusSituation
    extends
        NonNullableMutable<CssBackgroundStatusSituation>
{

    MutableCssBackgroundStatusSituation(CssBackgroundStatusSituation value) : super(value);

}

import 'package:pub_base_model/model.dart';
import 'package:pub_base_model/num_state_model_action.dart';

class ChecksStatusSize
    extends
        NumStateModel<num>
    with
        NumStateModelGetPx<num>
{

    /// checks の大きさ
    /// 単位は px.
    ChecksStatusSize(this.value);

    @override
    final num value;

}

import 'package:pub_base_model/model.dart';

class CssBackgroundStatusBorderColor
    extends
        StringStateModel
{

    CssBackgroundStatusBorderColor(this.value);

    @override
    final String value;

}
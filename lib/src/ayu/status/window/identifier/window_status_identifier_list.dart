
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/computation.dart';

import 'window_status_identifier.dart';

class WindowStatusIdentifierList
    extends
        ListComputation<
            WindowStatusIdentifier
            ,WindowStatusIdentifierList
            ,SampleDartWebAngularModule
        >
{

    WindowStatusIdentifierList(this.values);

    @override
    final Iterable<WindowStatusIdentifier> values;

    @override
    WindowStatusIdentifierList internalFactory(Iterable<WindowStatusIdentifier> models) => WindowStatusIdentifierList(models);

}
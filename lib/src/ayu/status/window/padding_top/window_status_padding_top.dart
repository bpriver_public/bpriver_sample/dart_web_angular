
import '../window_status_barrel.dart';

class WindowStatusPaddingTop
    extends
        WindowStatusBarrel<
            WindowStatusPaddingTop
        >
{

    /// 0 から 10000 までの整数（int 型）
    /// 単位は px.    
    WindowStatusPaddingTop(this.value);

    @override
    final int value;

    @override
    WindowStatusPaddingTop internalFactory(int value) => WindowStatusPaddingTop(value);

}

import '../window_status_barrel.dart';

class WindowStatusPaddingRight
    extends
        WindowStatusBarrel<
            WindowStatusPaddingRight
        >
{

    /// 0 から 10000 までの整数（int 型）
    /// 単位は px.    
    WindowStatusPaddingRight(this.value);

    @override
    final int value;

    @override
    WindowStatusPaddingRight internalFactory(int value) => WindowStatusPaddingRight(value);

}
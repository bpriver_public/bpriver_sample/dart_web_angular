
import 'package:sample_dart_web_angular/src/ayu/status/sprite_background/sprite_background_status.dart';
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/model.dart';

import 'window_status_barrel.dart';

export 'window_status_list.dart';
export 'window_status_barrel.dart';

class WindowStatus
    extends
        ObjectModel<SampleDartWebAngularModule>
{

    WindowStatus(this.identifier, this.width, this.height, this.x, this.y, this.paddingTop, this.paddingBottom, this.paddingLeft, this.paddingRight,
        {
            this.spriteSituation = const SpriteBackgroundStatusSituation.initial(),
        }
    );
    final WindowStatusIdentifier identifier;    
    final WindowStatusWidth width;
    final WindowStatusHeight height;
    final WindowStatusXAxis x;
    final WindowStatusYAxis y;
    final WindowStatusPaddingTop paddingTop;
    final WindowStatusPaddingBottom paddingBottom;
    final WindowStatusPaddingLeft paddingLeft;
    final WindowStatusPaddingRight paddingRight;
    final SpriteBackgroundStatusSituation spriteSituation;

    WindowStatus changeIdentifier(WindowStatusIdentifier identifier) {
        return WindowStatus(identifier, width, height, x, y, paddingTop, paddingBottom, paddingLeft, paddingRight, spriteSituation: spriteSituation);
    }

    WindowStatus changeX(WindowStatusXAxis x) {
        return WindowStatus(identifier, width, height, x, y, paddingTop, paddingBottom, paddingLeft, paddingRight, spriteSituation: spriteSituation);
    }

    WindowStatus changeY(WindowStatusYAxis y) {
        return WindowStatus(identifier, width, height, x, y, paddingTop, paddingBottom, paddingLeft, paddingRight, spriteSituation: spriteSituation);
    }

    WindowStatus changeSpriteSituation(SpriteBackgroundStatusSituation spriteSituation) {
        return WindowStatus(identifier, width, height, x, y, paddingTop, paddingBottom, paddingLeft, paddingRight, spriteSituation: spriteSituation);
    }


    @override
    Map<String, String> toPrimitive() {
        return {
            'identifier': identifier.toString(),
            'width': width.toString(),
            'height': height.toString(),
            'x': x.toString(),
            'y': y.toString(),
            'paddingTop': paddingTop.toString(),
            'paddingBottom': paddingBottom.toString(),
            'paddingLeft': paddingLeft.toString(),
            'paddingRight': paddingRight.toString(),
            'spriteSituation': spriteSituation.toString(),
        };
    }

}
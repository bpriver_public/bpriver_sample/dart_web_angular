
import 'package:sample_dart_web_angular/src/ayu/status/focus/x_axis/focus_status_x_axis.dart';
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/model.dart';

import 'height/focus_status_height.dart';
import 'width/focus_status_width.dart';
import 'y_axis/focus_status_y_axis.dart';

export 'height/focus_status_height.dart';
export 'width/focus_status_width.dart';
export 'x_axis/focus_status_x_axis.dart';
export 'y_axis/focus_status_y_axis.dart';

class FocusStatus
    extends
        ObjectModel<SampleDartWebAngularModule>
{

    FocusStatus(this.width, this.height, this.x, this.y);

    final FocusStatusWidth width;
    final FocusStatusHeight height;
    final FocusStatusXAxis x;
    final FocusStatusYAxis y;
    
    @override
    Map<String, String> toPrimitive() {
        return {
            'width': width.toString(),
            'height': height.toString(),
            'x': x.toString(),
            'y': y.toString(),
        };
    }

}
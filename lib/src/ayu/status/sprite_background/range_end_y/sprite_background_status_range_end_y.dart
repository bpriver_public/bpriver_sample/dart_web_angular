
import 'package:pub_base_model/model.dart';

class SpriteBackgroundStatusRangeEndY
    extends
        IntStateModel
{

    SpriteBackgroundStatusRangeEndY(this.value);

    @override
    final int value;

}
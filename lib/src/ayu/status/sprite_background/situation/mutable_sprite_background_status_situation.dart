
import 'package:pub_base_model/mutable.dart';

import 'sprite_background_status_situation.dart';

class MutableSpriteBackgroundStatusSituation
    extends
        NonNullableMutable<SpriteBackgroundStatusSituation>
{

    MutableSpriteBackgroundStatusSituation(SpriteBackgroundStatusSituation value) : super(value);

}
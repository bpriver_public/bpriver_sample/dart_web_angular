
import 'package:pub_base_model/model.dart';

class SpriteBackgroundStatusRangeBeginX
    extends
        IntStateModel
{

    SpriteBackgroundStatusRangeBeginX(this.value);

    @override
    final int value;

}
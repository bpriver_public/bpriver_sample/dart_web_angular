
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/between_x_and_y.dart';
import 'package:pub_base_model/model.dart';

export 'sprite_background_status_turn_list.dart';

/// sprite animation の particle を再生する順番を決定する
class SpriteBackgroundStatusTurn
    extends
        IntStateModel
    with
        BetweenXAndY<
            int
            ,SpriteBackgroundStatusTurn
            ,SampleDartWebAngularModule
        >
        ,Between0And10000<
            int
            ,SpriteBackgroundStatusTurn
            ,SampleDartWebAngularModule
        >
{

    SpriteBackgroundStatusTurn(this.value);

    @override
    final int value;

}

import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/between_x_and_y.dart';
import 'package:pub_base_model/model.dart';

class SpriteBackgroundStatusDuration
    extends
        IntStateModel
    with
        BetweenXAndY<
            int
            ,SpriteBackgroundStatusDuration
            ,SampleDartWebAngularModule
        >
        ,Between0And10000<
            int
            ,SpriteBackgroundStatusDuration
            ,SampleDartWebAngularModule
        >
{

    /// 単位は milliseconds.
    SpriteBackgroundStatusDuration(this.value);

    @override
    final int value;

}
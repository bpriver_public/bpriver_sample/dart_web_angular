
import 'dart:html';

import 'package:angular/angular.dart';

import 'final_input_status.dart';

import 'final_input_status_directive.template.dart' as temp;

export 'final_input_status.dart';

/// 
/// 検証結果
/// 
/// @Input annotation がついた property は final にできない。
///     late keyword を使ってもダメ。
///         呼び出し側 で、[status] に値を入れていても、だめ
///     どうしても error になる。
/// 
/// @Input property が代入されるタイミング(早い順)
///     宣言時
///     Initializer list
///     Constructor
///     @Input [xxx]="xxx"
///     ngOnInit
///     
/// @Input annotaion がついた property を null safe にする。
///     パターン1   
///         ngOnInit で、??= で初期値を設定
///     パターン2
///         宣言時に初期値を設定
///         ＠Input で入力されれば、上書きされる
///     パターン3
///         late keyword をつける
///         ＠Input で入力されなければ error がでる
///         なので、@Input への入力が強制される
/// 
@Component(
    selector: 'sample-final-input-status',
    template: '<ng-content></ng-content>',
    directives: [
    ],
    exports: [
    ],
)
class FinalInputStatusDirective
    implements
        OnInit
        ,AfterChanges
{

    static final template = temp.FinalInputStatusDirectiveNgFactory;

    FinalInputStatusDirective(this.el)
    // :
    //     status = FinalInputStatus('Initializer list')
    {
        // status = FinalInputStatus('Constructor');
        // status ??= FinalInputStatus('Constructor');
    }
    // ;

    final HtmlElement el;

    @Input()
    late FinalInputStatus status;
    // late final FinalInputStatus status;
    // FinalInputStatus status = FinalInputStatus('default');
    // FinalInputStatus? status;

    void elStyle() {
        el.text = status?.value;
    }

    @override
    void ngOnInit() {

        // status = FinalInputStatus('ngOnInit');
        // status ??= FinalInputStatus('ngOnInit');

        el.style.display = 'block';
        elStyle();
        print(status);
    }

    // @Input property が変更されたときに呼び出される
    @override
    void ngAfterChanges() {
        // status = FinalInputStatus('ngAfterChanges');
        // status ??= FinalInputStatus('ngOnInit');
        elStyle();
        print('$runtimeType : excute ngAfterChanges()');
    }

}